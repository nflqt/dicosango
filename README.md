
# Jeu pour apprendre le vocabulaire du Sango à partir du Français # 

Coucou tout le monde ! J'apprends -- enfin ! la programmation. Et
J'essaie de me créer des petits jeux pour apprendre le *Sango*, la
langue officielle de la
[Centrafrique](https://fr.wikipedia.org/wiki/R%C3%A9publique_centrafricaine).

Les outils utilisés sont
+ **GNU/Linux**
+ **Python** avec notamment
  + pyflakes
  + black
  + pylint
  + pydocstyle
+ **Emacs** avec notamment les modes
  + Python
  + Markdown
  + Magit


