#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright 2023 Nicolas Floquet

# This file is part of DicoSango.
#
# DicoSango is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DicoSango is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with DicoSango. If not, see <https://www.gnu.org/licenses/>.

"""Pose des question de vocabulaire entre le Sango et le Français.

Le Sango est la langue officielle de la Centrafrique, tandis que le
Français est la langue officielle de la France ainsi que de quelques
autres États.

"""

import random

## Dictionnaire ##

dictionnaire = (
    ["Terê", "Corps"],
    ["Poro ti terê", "Peau"],
    ["Li", "Tête"],
    ["Singila", "Merci"],
    ["Da", "Maison"],
    ["Nioda", "Porte"],
    ["Doporo", "Fenêtre"],
    ["Mêza", "Table"],
    ["Ngende", "Chaise"],
    ["Gbogbo", "Lit"],
    ["Singa-me", "Téléphone"],
    ["Singa-le", "Télévision"],
    ["Kutukutu", "Véhicule"],
    ["Gbazabanga", "Vélo"],
    ["Bada", "Voiture"],
    ["Masua", "Bateau"],
    ["Lapara", "Avion"],
    ["Mangbèrè", "Chikwangue"],
    ["Kondo", "Poulet"],
    ["Kanäna", "Canard"],
    ["Ngasa", "Chèvre"],
    ["Taba", "Mouton"],
    ["Ngundë", "Crocodile"],
    ["Bamara", "Lion"],
    ["Zebre", "Zèbre"],
    ["Doli", "Éléphant"],
    ["Tule", "Araignée"],
    ["Mini", "Fourmi"],
    ["Wali", "Femme"],
    ["Pendere", "Beau"],
)

## Programme ##

# Choix du type de test #

print(
    'Tapez "V" (pour version) si vous souhaitez tester votre\
vocabulaire du sango vers le français. \nTapez "T" (pour thème), si\
vous souhaitez tester du français vers le sango.'  )

version_ou_theme = input("reponse [V/T] : ")
while version_ou_theme not in ("V", "T"):
    print("Vous avez du faire une faute de frappe.")
    version_ou_theme = input("Retapez votre réponse : ")

# Test #

score = 0
nombre = 10
hasard = random.sample(dictionnaire, nombre)

if version_ou_theme == "V":
    while score < nombre:
        print(f"Quelle est la traduction de {hasard[score][0]} ?")
        reponse = hasard[score][1]

        question = input("reponse : ")
        while question != hasard[score][1]:
            print("Faux ! Essaie encore. ")
            question = input("reponse : ")
        score = score + 1
        print(f"{score}/{nombre}", end=" ")
    print("Bravo !")
else:
    while score < 10:
        print(f"Quelle est la traduction de {hasard[score][1]} ?")
        reponse = hasard[score][0]

        question = input("reponse : ")
        while question != hasard[score][0]:
            print("Faux ! Essaie encore. ")
            Question = input("reponse : ")
        score = score + 1
        print(f"{score}/{nombre}", end=" ")
    print("Bravo !")
